/**
 * Задание 3
 * 
 * Переписать код используя короткую форму записи и исправить ошибки.
 */

// const myValue = 2;
//
// myValue = myValue + 2;
//
// myValue = myValue + 1;
//
// myValue = myValue - 10;
//
// const data = myValue * myValue * myValue;

let myValue = 2; // const to let так как const не изменяется

myValue += 2;
// console.log(myValue); // 4 (2+2)

myValue += 1;
// console.log(myValue); // 5 (4+1)

myValue -= 10;
// console.log(myValue); // -5 (5-10)

const data = myValue ** 3; // в степень
// let data = myValue **= 3; // можно еще так но это выглядит "извращением")

console.log(data); // -125