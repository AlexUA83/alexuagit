/**
 * Задание 1
 * 
 * Написать скрипт сложения, вычитания, умножения и деления двух чисел.
 * Результат каждой операции должен быть записан в переменную и выведен в консоль.
 * 
 * Пример: sum1 = 1
 *         sum2 = 2
 *         результуть сложения: 3
 *         результуть вычитания: -1
 *         результуть умножения: 2
 *         результуть деления: 0.5
 */
const sum1 = prompt('Введите первое число:'); // ЗАПРЕЩЕНО МЕНЯТЬ
const sum2 = prompt('Введите второе число:'); // ЗАПРЕЩЕНО МЕНЯТЬ

// РЕШЕНИЕ
const numOne = Number(sum1);
const numTwo = Number(sum2);
let addition = numOne + numTwo;
let subtraction = numOne - numTwo;
let multiplication = numOne * numTwo;
let division = numOne / numTwo;
console.log('Результат сложения: ' + addition);
console.log('Результат вычитания: ' + subtraction);
console.log('Результат умножения: ' + multiplication);
console.log('Результат деления: ' + division);
