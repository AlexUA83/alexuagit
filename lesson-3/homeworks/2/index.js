// Перепишите `if..else` с использованием нескольких операторов `?`. 
// Для читаемости — оформляйте код в несколько строк.

////////////////// Задание //////////////////
// let message;

// if (login === 'Pitter') {
//   message = 'Hi';
// } else if (login === 'Owner') {
//   message = 'Hello';
// } else if (login === '') {
//   message = 'unknown';
// } else {
//   message = '';
// }

////////////////// Решение //////////////////
const login = 'Owner'; // В условии задания login почему-то не был инициализирован

const message = (login === 'Pitter') ? 'Hi' :
    (login === 'Owner') ? 'Hello' :
        (login === '') ? 'unknown' :
            '';

console.log(message); // Так же в условии вывода никакого не было
