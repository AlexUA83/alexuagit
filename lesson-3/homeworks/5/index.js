/**
 * Задание 5
 * 
 * Используя цикл найти факториал числа.
 * Факториал числа вывести в консоль.
 */

const number = prompt('Введите число:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

//РЕШЕНИЕ
// 5! = 5 * 4 * 3 * 2 * 1 = 120
const num = Number(number);

if (isNaN(num) || num < 0) {
    console.log('Error! Вы ввели что-то не то!');
}
// else if (num < 0) {
//     console.log('Error! Факториала для отрицательного числа не существует.');
// }
else if (num === 0) {
    console.log('Факториал для числа 0 будет 1.');
}
else {
    let factorial = 1;
    for (let i = 1; i <= num; i++) {
        factorial *= i;
    }
    console.log('Факториалом числа ' + num + ' будет ' + factorial);
}
