// Перепишите код, заменив оператор 'if' на тернарный оператор '?'

////////////////// Задание //////////////////
// const a = 2;
// const b = 1;
// let result = null;

// if (a + b < 4) {
//     result = true;
// } else {
//     result = false;
// }

//console.log(result);
////////////////// Решение //////////////////
const a = 2;
const b = 1;

const result = a + b < 4 ? true : false;
// const result = a + b < 4;  // WebStorm советует делать так, оно то и понятно, что результатом сравнения и так
// будет или true или false и указывать явно результат не обьязательно
console.log(result);
