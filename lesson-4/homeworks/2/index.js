/*
 * Задача 2.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо присвоить переменной result значение true, 
 * если строка source содержит подстроку spam. Иначе — false.
 * 
 * Пример 1: source -> pitterXXX@gmail.com, 
 *           spam -> 'xxx',
 *           result -> true;
 * 
 * Пример 2: source -> pitterXXX@gmail.com, 
 *           spam -> 'XXX',
 *           result -> true;
 * 
 * Пример 3: source -> pitterXXX@gmail.com, 
 *           spam -> 'sss',
 *           result -> false;
 *
 * Условия:
 * - Строки должны быть не чувствительными к регистру
 * - Нужно использовать метод include() 
 *   https://developer.mozilla.org/ru/docs/orphaned/Web/JavaScript/Reference/Global_Objects/String/includes
 */

const source = prompt('Введите электропочту:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
const spam = prompt('Введите строку, которую нужно найти в электропочте'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
// let result = null;

// РЕШЕНИЕ
/* Так как Метод includes() является регистрозависимым то буду приводить и source и spam к одному виду,
    думаю тут без разницы хоть в .toLowerCase() хоть в .toUpperCase()*/

// if (source.toUpperCase().includes(spam.toUpperCase())) {
//     const result = true;
//     console.log(result);
// } else {
//     const result = false;
//     console.log(result);
// }


//// вот так кратко и имхо красиво тоже отрабатывает
const result = source.toUpperCase().includes(spam.toUpperCase())
console.log(result);
