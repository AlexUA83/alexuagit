/*
 * Задача 3.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 * 
 * Пример 1: string -> 'Вот, что мне хотелось бы сказать на эту тему:'
 *         maxLength -> 21
 *         result -> 'Вот, что мне хотел...'
 * 
 * Пример 2: string -> 'Вот, что мне хотелось'
 *         maxLength -> 100
 *         result -> 'Вот, что мне хотелось'
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 * 
 */
const string = prompt('Введите строку, которую нужно сократить:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
const maxLength = prompt('Введите максимальную длинну строки:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ
// Вариант 1

// if (maxLength && string.length > maxLength) {
//     const numLength = Number(maxLength);
//     newStr = string.substring(0, numLength - 3) + '...';
//     console.log(newStr);
// }
// else {
//     console.log(string);
// }

// Вариант 2

if (maxLength === null){
    console.log('Вы не ввели желаемую длинну!');
}
else if (string.length > maxLength) {
    const numLength = Number(maxLength);
    newStr = string.substring(0, numLength - 3) + '...';
    console.log(newStr);
}
else {
    console.log(string);
}