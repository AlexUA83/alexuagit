/*
 * Задача 2.
 *
 * Создайте объект `user`, со свойствами `name`, `surname`, `job` и `data`.
 * 
 * При чтении свойства `data` должна возвращаться строка с текстом.
 * Возвращаемая строка должна содержать текст: `Привет! Я `name` `surname` и я работаю `job` `.
 * 
 * 
 * Значения свойств `name`, `surname`, `job` в объекте `user` нужно получать из функции prompt().
 * 
 * Условия:
 * - Свойство `data` обязательно должно быть геттером. 
 * 
 * Обратите внимание!
 * - Для того что бы обратиться к свойству оъекта необходимо использовать this.name, this.surname и this.job. * 
 */

// РЕШЕНИЕ
const nameInput = prompt('Введите имя:');
const surnameInput = prompt('Введите фамилию:');
const jobInput = prompt('Введите компанию:');

const user = {};

Object.defineProperties(user, {
    name: {
        value: nameInput,
        writable: true
    },
    surname: {
        value: surnameInput,
        writable: true
    },
    job: {
        value: jobInput,
        writable: true
    },
    data: {
        get() {
            return `Привет! Я ${this.name} ${this.surname} и я работаю в ${this.job}`;
        }
    }
});

console.log(user.data);

/* Если я вдруг вылез за рамки ТЗ и самовольно применил Object.defineProperties
Было уже такое замечание мне 🙈
То в более простом варианте будет так: */

// const user = {
//     name: nameInput,
//     surname: surnameInput,
//     job: jobInput,
//     get data() {
//         return `Привет! Я ${this.name} ${this.surname} и я работаю ${this.job}`;
//     }
// };
// console.log(user.data);
